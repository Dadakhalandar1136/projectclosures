function counterFactory() {

    let counter = 0;
    const increment = function () {
        counter++;
        return counter;
    }

    const decrement = function () {
        counter--;
        return counter;
    }

    return {
        increment,
        decrement
    }
}

module.exports = counterFactory;