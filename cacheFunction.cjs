function cacheFunction(cb) {
    let cache = {};
    if (typeof cb != "function") {
        throw new Error("Partial parameters passed ");
    }

    let invoking = function (...argumentsPresent) {
        if (cache[argumentsPresent]) {
            return cache[argumentsPresent];
        } else {
            cache[argumentsPresent] = cb(...argumentsPresent);
            return cache[argumentsPresent];
        }
    }
    return invoking;
}

module.exports = cacheFunction;