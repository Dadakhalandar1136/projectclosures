const cacheFunction = require('../cacheFunction.cjs');

let callbackCache = function (cacheValue) {
    return cacheValue;
}

try {
    const returnValue = cacheFunction(callbackCache);
    console.log(returnValue(1, 2, 3, 4));
    console.log(returnValue(1, 2, 3, 4));
    console.log(returnValue(1, 2, 3, 4));
} catch (error) {
    console.error(error);
}