const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function cb(number) {
    return number;
}
try {
    const returnValue = limitFunctionCallCount(cb, 2);
    console.log(returnValue(1, 4, 'heloo'));
    console.log(returnValue(1, 4));
    console.log(returnValue(1, 4));
} catch (error) {
    console.error(error);
}

