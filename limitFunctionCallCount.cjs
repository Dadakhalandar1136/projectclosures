function limitFunctionCallCount(cb, n) {

    if (isNaN(n) || n == undefined || typeof cb !== 'function') {
        throw new Error('Parameter is not a number or undefined');
    }
    let numLimit = 0;
    function repeatValue(...args) {
        if (numLimit < n) {
            numLimit += 1;
            return cb(...args);
        } else {
            return null;
        }
    }
    return repeatValue;
}

module.exports = limitFunctionCallCount;